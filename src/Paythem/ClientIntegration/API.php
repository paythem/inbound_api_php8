<?php

namespace Paythem\ClientIntegration;

use Error;
use Exception;
use RuntimeException;
use Throwable;

/**
 * This class is used for direct integration with the PayThem.Net Electronic Voucher Distribution API subsystem.
 *
 * @contact support@paythem.atlassian.net
 * @version 7.7.1
 * @copyright PayThem.Net WLL, 2014-2024
 *
 * @requires Requires openSSL.
 */
class API
	extends baseIntegration {
	/**
	 * @var int the returned API call ID, as returned by server, representing
	 * the current call's log entry on the server.
	 */
	public int $serverTransactionID                 = 0;

	/**
	 * @var string $resultCode the returned API result code. 00000 if success, otherwise,
	 * a relevant error ID. Description is returned and stored in $errorDescription and
	 * response['ERROR_DESCRIPTION'] and result['ERROR_DESCRIPTION'].
	 */
	public string $resultCode						= '';

	/**
	 * @var string $httpResponse the original HTTP response.
	 */
	public string $httpResponse;

	/**
	 * @var string $curlHTTPError the cURL code, if a cURL error occurs.
	 */
	public string $curlHTTPError;

	/**
	 * @var string $errorDescription the translated error code to human legible text as returned from server.
	 */
	public string $errorDescription;

	/**
	 * @var mixed $result the result as JSON-decoded and, optionally, decrypted, associative array.
	 */
	public mixed $result;

	/**
	 * @var ?array $response the raw response, as received from server, as a string
	 */
	public ?array $response;

	/**
	 * @var string $API_VERSION Current API Version
	 */
	private string $API_VERSION                     = '2.2.8';

	/**
	 * @var string $CLIENT_VERSION Current client version.
	 */
	private string $CLIENT_VERSION                = '7.7.1';

	/**
	 * @var false|resource $cURLing The cURL object, instantiated once in constructor, for multiple calls.
	 */
	private $cURLing;

	/**
	 * @var array $innerVars the inner variables for setting storage.
	 */
	protected array $innerVars          				= [];

	/**
	 * @var string $hashMacENC encoding used for HMAC hash creation.
	 */
	private string $hashMacENC                      = 'sha256';

	/**
	 * @var string $serverURI The URI / Universal Resource Identifier of the server.
	 */
	private string $serverURI;

	/**
	 * @var bool $ERROR flag to easily identify if an error occurred from the calling function.
	 */
	public bool $ERROR;

	/**
	 * @var array $encryptedContent Associative array containing the encrypted content to be JSON encoded and then posted to server.
	 */
	private array $encryptedContent                 = [];

	/**
	 * @var string $curlErrorString cURL error explanation. Empty when no cURL error.
	 */
	public string $curlErrorString;

	/**
	 * @var string $curlResponseCode cURL response code.
	 */
	public string $curlResponseCode;

	/**
	 * @var int $curlHTTPResponseCode HTTP response code.
	 */
	public int $curlHTTPResponseCode;

	/**
	 * Constructor.
	 *
	 * @param string $environment The server environment that this transaction will be executed against.
	 * @param int    $appID       The API Application to communicate with.
	 * @param string $publicKey   Public key, provided by PayThem
	 * @param string $privateKey  Private key, , provided by PayThem
	 * @param string $userName    Username, provided by PayThem
	 * @param string $password    Password, provided by PayThem
	 */
	public function __construct(
		string $environment,
		int $appID,
		string $publicKey,
		string $privateKey,
		string $userName,
		string $password
	) {
		if (!extension_loaded('curl')) {
			$this->ERROR                            = true;

			throw new Error('PHP curl extension not loaded.');
		}
		if (!extension_loaded('openssl')) {
			$this->ERROR                            = true;

			throw new Error('PHP OPENSSL extension not loaded.');
		}

		$this->serverURI            				= "https://vvs$environment.paythem.net/API/$appID/";
		$this->innerVars['PARAMETERS']				= [];

		$this->cURLing								= curl_init();
		curl_setopt($this->cURLing, CURLOPT_URL				, $this->serverURI);
		curl_setopt($this->cURLing, CURLOPT_POST    		, 1);
		curl_setopt($this->cURLing, CURLOPT_RETURNTRANSFER	, true);
		curl_setopt($this->cURLing, CURLOPT_SSL_VERIFYPEER	, true);

		$this->innerVars                            = [
			'ENCRYPT_RESPONSE'                      => true,
			'PUBLIC_KEY'                            => !empty($publicKey)  ? $publicKey  : throw new Error('Public key cannot be empty.'),
			'PRIVATE_KEY'                           => !empty($privateKey) ? $privateKey : throw new Error('Private key cannot be empty.'),
			'USERNAME'                              => !empty($userName)   ? $userName   : throw new Error('Username cannot be empty.'),
			'PASSWORD'                              => !empty($password)   ? $password   : throw new Error('Password cannot be empty'),
			'API_VERSION'                           => $this->API_VERSION,
			'CLIENT_VERSION'                        => $this->CLIENT_VERSION,
			'CLIENT_LANGUAGE'                       => 'PHP',
			'CLIENT_LANGUAGE_VERSION'               => phpversion(),
			'SERVER_URI'                            => $this->serverURI,
			'SERVER_TIMEZONE'                       => date_default_timezone_get()
		];
	}

	/**
	 * Destructor
	 */
	public function __destruct() {
		try {
			@curl_close($this->cURLing);
		} catch (Throwable $e) {
		
		} finally {
			unset(
				$e, $this->cURLing, $this->innerVars, $this->serverTransactionID,
				$this->resultCode, $this->httpResponse, $this->curlHTTPError,
				$this->result
			);
		}
	}

	/**
	 * Magic method for returning from innerVars.
	 *
	 * Leaving in for backwards compatibility.
	 *
	 * @param string $prop
	 *
	 * @return mixed|null
	 */
	public function __get(string $prop): mixed {
		return $this->innerVars[$prop] ?? NULL;
	}

	/**
	 * Magic method to set a value in innerVars.
	 *
	 * Leaving in for backwards compatibility.
	 *
	 * @param string $prop
	 * @param mixed $val
	 *
	 * @return void
	 */
	public function __set(string $prop, mixed $val): void {
		$this->innerVars[$prop]                     = $val;
	}

	/**
	 * Magic method to determine if variable is set.
	 *
	 * Leaving in for backwards compatibility.
	 *
	 * @param string $name of variable
	 *
	 * @return bool
	 */
	public function __isset(string $name): bool {
		return isset($this->innerVars[$name]);
	}

	/**
	 * Perform the call to the server.
	 *
	 * @return ?array
	 * @throws Exception
	 */
	public function callAPI(): ?array {
		$this->ERROR                                = false;

		// Reset cURL results.
		$this->curlHTTPError                        = 0;
		$this->curlErrorString                      = '';
		$this->curlResponseCode                     = 0;
		$this->curlHTTPResponseCode                 = 0;

		// Reset previously returned results;
		$this->httpResponse						    = '';
		$this->resultCode                           = '';
		$this->response                             = [
			'RESULT'                                => '',
			'SERVER_TRANSACTION_ID'                 => 0,
			'SYSLOG_ID'                             => 0,
			'ERROR_DESCRIPTION'                     => '',
			'CONTENT'                               => '',
		];
		$this->result                               = [];
		$this->errorDescription                     = '';

		// Fill in incomplete variables
		if (!isset($this->innerVars['ENCRYPT_RESPONSE'])) {
			$this->innerVars['ENCRYPT_RESPONSE']    = true;
		}
		// Check for incomplete variables
		if (!isset($this->innerVars['FUNCTION'])) {
			$this->setErrorResponse('ERROR API_C_00008: No function specified.', '-0002');

			throw new RuntimeException('ERROR API_C_00008: No function specified.');
		}

		// Build the content (POST) variable
		$content                                    = json_encode(
			array_merge(
				$this->innerVars,
				[
					'SERVER_TIMESTAMP'              => date('Y-m-d H:i:s'),
					'HASH_STUB'                     => random_int(1111111111, 9999999999),
					'PRIVATE_KEY'                   => 'REMOVED'
				]
			),
			JSON_THROW_ON_ERROR
		);

		// Generate the HMAC hash
		$hash										= hash_hmac($this->hashMacENC, $content, $this->innerVars['PRIVATE_KEY']);

		// Create the headers for the POST
		$headers									= [
			'X-Public-Key: '						.$this->innerVars['PUBLIC_KEY'],
			'X-Hash: '								.$hash
		];

		// Redo initialization vector
		$IV                                         = openssl_random_pseudo_bytes(16, $strong_result);

		if(
				$IV === '' ||
				!$strong_result
		) {
			$IV                                     = str_pad((string)random_int(1,9999999999999999), '0', STR_PAD_LEFT);
		}

		// Encrypt the POST content and set the PUBLIC KEY
		$this->encryptedContent['PUBLIC_KEY']       = $this->innerVars['PUBLIC_KEY'];
		$this->encryptedContent['CONTENT']          = $this->doEncrypt($content, $IV);

		// Do cURL call
		curl_setopt($this->cURLing, CURLOPT_POSTFIELDS		, $this->encryptedContent);
		curl_setopt($this->cURLing, CURLOPT_HTTPHEADER		, $headers);

		$this->httpResponse							= curl_exec($this->cURLing);

		// Immediately remove the encrypted content.
		$this->encryptedContent                     = [];

		// Fill in some blanks for cURL
		$this->curlResponseCode                     = curl_getinfo($this->cURLing, CURLINFO_RESPONSE_CODE);
		$this->curlHTTPError                        = curl_errno($this->cURLing);
		$this->curlErrorString                      = curl_error($this->cURLing);

		if ($this->curlHTTPError) {
			$this->setErrorResponse($this->curlErrorString);

			return $this->response;
		}

		// Process and decrypt the result
		$this->response								= json_decode($this->httpResponse, true);

		// Check response decoded;
		if (empty($this->response)) {
			$this->setErrorResponse('Could not decode server response.');

			return $this->response;
		}

		$this->resultCode						    = $this->response['RESULT'];
		$this->serverTransactionID					= $this->response['SERVER_TRANSACTION_ID'];

		if ((string)$this->resultCode !== '00000') {
			$this->setErrorResponse($this->response['ERROR_DESCRIPTION'], (string)$this->resultCode);
		} else {
			$this->result                           = $this->response['CONTENT']
													=
				json_decode(
					$this->innerVars['ENCRYPT_RESPONSE']
						? $this->doDecrypt($this->response['CONTENT'], $IV)
						: $this->response['CONTENT'],
					true
				);
		}

		return $this->response;
	}

	/**
	 * Set global error context / status / descriptions.
	 *
	 * @param string $msg
	 * @param string $code
	 *
	 * @return void
	 */
	private function setErrorResponse(string $msg, string $code='-0001'): void {
		$this->encryptedContent                 = [];
		$this->ERROR                            = true;
		$this->response['RESULT']               = $this->resultCode
												= $code;
		$this->response['CONTENT']              = $this->result
												= 'ERROR';
		$this->errorDescription                 = $this->response['ERROR_DESCRIPTION']
												= $msg;
	}

	/**
	 * Encrypt, using supplied credentials and settings, the passed content.
	 *
	 * @param string $content
	 * @param string $IV
	 *
	 * @return string
	 */
	private function doEncrypt(string $content, string $IV): string {
		$this->encryptedContent['ENC_METHOD']       = 'AES-CBC-256-OPENSSL';
		$this->encryptedContent['ZAPI']             = $IV;

		return base64_encode((string)openssl_encrypt($content,'aes-256-cbc', $this->innerVars['PRIVATE_KEY'],OPENSSL_RAW_DATA, $IV));
	}

	/**
	 * Decrypt, using supplied credentials and settings, passed content.
	 *
	 * @param string $content
	 * @param string $IV
	 *
	 * @return string
	 */
	private function doDecrypt(string $content, string $IV): string {
		return (string)openssl_decrypt(base64_decode($content), 'aes-256-cbc', $this->innerVars['PRIVATE_KEY'], OPENSSL_RAW_DATA, $IV);
	}

	/**
	 * Begin API call functions
	 * From version 2.2.8
	 */

	/**
	 * Retrieve an Original Equipment Manufacturer (OEM) List
	 *
	 * @return ?array
	 * @throws Exception
	 */
	public function get_OEMList(): ?array {
		$this->innerVars['FUNCTION']				= __FUNCTION__;

		return $this->callAPI();
	}

	/**
	 * Get Brand List
	 *
	 * Retrieve a list of brands
	 *
	 * @return array|null
	 * @throws Exception
	 */
	public function get_BrandList(): ?array {
		$this->innerVars['FUNCTION']				= __FUNCTION__;

		return $this->callAPI();
	}

	/**
	 * Get Product List
	 *
	 * Retrieve a list of all products
	 *
	 * @return array|null
	 * @throws Exception
	 */
	public function get_ProductList(): ?array {
		$this->innerVars['FUNCTION']				= __FUNCTION__;

		return $this->callAPI();
	}

	/**
	 * Get Account Balance
	 *
	 * Retrieve the current balance of profile
	 *
	 * @return array|null
	 * @throws Exception
	 */
	public function get_AccountBalance(): ?array {
		$this->innerVars['FUNCTION']				= __FUNCTION__;

		return $this->callAPI();
	}

	/**
	 * Get Vouchers
	 *
	 * Purchase the specified amount of vouchers for the specified product
	 *
	 * @param int    $productId The ID of the product as retrieved from get_ProductList
	 * @param int    $quantity  The quantity of vouchers to purchase (Defaults to 1)
	 * @param string $reference The reference ID for the transaction
	 *
	 * @return array|null
	 * @throws Exception
	 */
	public function get_Vouchers(int $productId, int $quantity = 1, string $reference = ''): ?array {
		$this->innerVars['FUNCTION']				= __FUNCTION__;
		$this->innerVars['PARAMETERS']['PRODUCT_ID']	= $productId;
		$this->innerVars['PARAMETERS']['QUANTITY']		= $quantity;
		$this->innerVars['PARAMETERS']['REFERENCE_ID']	= $reference;
		
		return $this->callAPI();
	}

	/**
	 * Handle date calculations for date based calls.
	 *
	 * @param string $fromDate
	 * @param string $toDate
	 *
	 * @return array|null
	 * @throws Exception
	 */
	private function dateBasedCall(string $fromDate, string $toDate=''): ?array {
		if (strlen($fromDate) < 10) {
			return [
				'RESULT'                            => '-0001',
				'CONTENT'                           => 'Incorrect from date length.'
			];
		} elseif (strlen($toDate) === 10) {
			$fromDate                               = $fromDate.' 00:00:00';
		}

		$this->innerVars['PARAMETERS']['FROM_DATE']	= $fromDate;
		$this->innerVars['PARAMETERS']['TO_DATE']	= (!empty($toDate) ? $toDate : date('Y-m-d')).' 23:59:59';

		return $this->callAPI();
	}

	/**
	 * Get Financial Transaction By Date Range
	 *
	 * Get all financial transactions (deposits, credits, reversals, purchases, etc.) for company
	 *
	 * @param string $fromDate The date from which to check, excluding the time (e.g. 2022-01-01)
	 * @param string $toDate   The date to which to check, excluding the time (e.g. 2022-01-31)
	 *
	 * @return array|null
	 * @throws Exception
	 */
	public function get_FinancialTransaction_ByDateRange(string $fromDate, string $toDate = ''): ?array {
		$this->innerVars['FUNCTION']				= __FUNCTION__;

		return $this->dateBasedCall($fromDate, $toDate);
	}

	/**
	 * Get Sales Transaction By Date Range
	 *
	 * Get all vouchers purchased during a specific period for API user
	 *
	 * @param string $fromDate The date from which to check, excluding the time (e.g. 2022-01-01)
	 * @param string $toDate   The date to which to check, excluding the time (e.g. 2022-01-31)
	 *
	 * @return array|null
	 * @throws Exception
	 */
	public function get_SalesTransaction_ByDateRange(string $fromDate, string $toDate = ''): ?array {
		$this->innerVars['FUNCTION']				= __FUNCTION__;

		return $this->dateBasedCall($fromDate, $toDate);
	}

	/**
	 * Get Product Availability
	 *
	 * Retrieve the availability of the specified product
	 *
	 * @param int $productId The ID of the product as retrieved from get_ProductList
	 *
	 * @return array|null
	 * @throws Exception
	 */
	public function get_ProductAvailability(int $productId): ?array {
		$this->innerVars['FUNCTION']				= __FUNCTION__;
		$this->innerVars['PARAMETERS']['PRODUCT_ID']= $productId;

		return $this->callAPI();
	}

	/**
	 * Get All Product Availability
	 *
	 * Retrieve the availability of all products
	 *
	 * @return array|null
	 * @throws Exception
	 */
	public function get_AllProductAvailability(): ?array {
		$this->innerVars['FUNCTION']				= __FUNCTION__;

		return $this->callAPI();
	}

	/**
	 * Get Product Formats
	 *
	 * Get all financial transactions (deposits, credits, reversals, purchases, etc) for company
	 *
	 * @return array|null
	 * @throws Exception
	 */
	public function get_ProductFormats(): ?array {
		$this->innerVars['FUNCTION']				= __FUNCTION__;

		return $this->callAPI();
	}
	
	/**
	 * Get Maximum Allowed Vouchers Per Call
	 *
	 * Retrieve the maximum allowed voucher purchase per call
	 *
	 * @return array|null
	 * @throws Exception
	 */
	public function get_MaxAllowedVouchersPerCall(): ?array {
		$this->innerVars['FUNCTION']				= __FUNCTION__;

		return $this->callAPI();
	}
	
	/**
	 * Get Product Info
	 *
	 * Retrieves information about the requested product
	 *
	 * @param int $productId The ID of the product as retrieved from get_ProductList
	 *
	 * @return array|null
	 * @throws Exception
	 */
	public function get_ProductInfo(int $productId): ?array {
		$this->innerVars['FUNCTION']				= __FUNCTION__;
		$this->innerVars['PARAMETERS']['PRODUCT_ID']	= $productId;

		return $this->callAPI();
	}

	/**
	 * Get Transaction By Transaction ID
	 *
	 * Retrieves all transactions for a given transaction ID
	 *
	 * @param string $transactionId The transaction ID returned during get_Vouchers purchase
	 *
	 * @return array|null
	 * @throws Exception
	 */
	public function get_SalesTransaction_ByTransactionId(string $transactionId): ?array {
		$this->innerVars['FUNCTION']				= __FUNCTION__;
		$this->innerVars['PARAMETERS']['TRANSACTION_ID']	= $transactionId;

		return $this->callAPI();
	}

	/**
	 * Get Transaction By Reference ID
	 *
	 * Retrieves all transactions for a given reference ID
	 *
	 * @param string $referenceId The reference ID passed during get_Vouchers
	 *
	 * @return array|null
	 * @throws Exception
	 */
	public function get_SalesTransaction_ByReferenceId(string $referenceId): ?array {
		$this->innerVars['FUNCTION']				= __FUNCTION__;
		$this->innerVars['PARAMETERS']['REFERENCE_ID']	= $referenceId;

		return $this->callAPI();
	}

	/**
	 * Get Last Sale
	 *
	 * Retrieve the information of the last sale that successfully processed
	 *
	 * @return array|null
	 * @throws Exception
	 */
	public function get_LastSale(): ?array {
		$this->innerVars['FUNCTION']				= __FUNCTION__;

		return $this->callAPI();
	}
}