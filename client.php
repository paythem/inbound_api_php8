<?php
/**
 * Example client implementation for PHP 8.0 and above.
 */

require_once __DIR__ . '/vendor/autoload.php';
use Paythem\ClientIntegration\API;

const PUBLIC_KEY = 'REQUEST FROM PAYTHEM';
const PRIVATE_KEY = 'REQUEST FROM PAYTHEM';
const USERNAME = 'REQUEST FROM PAYTHEM';
const PASSWORD = 'REQUEST FROM PAYTHEM';

try {
	$api					        = new API(
		'q',                                            // Environment: 'q' (non-public testing), 'demo' (sandbox) and '' (production)
		2824,                                           // API application endpoint
		constant('PUBLIC_KEY'),                         // Public key, supplied by PayThem
		constant('PRIVATE_KEY'),                        // Private key, supplied by PayThem
		constant('USERNAME'),                           // Username, supplied by PayThem
		constant('PASSWORD')                            // Password, supplied by PayThem
	);
	
	//$res = $api->get_AccountBalance();              // Has no parameters
	//$res = $api->get_OEMList();                     // Has no parameters
	//$res = $api->get_BrandList();                   // Has no parameters
	//$res = $api->get_ProductList();                 // Has no parameters
	//$res = $api->get_Vouchers(1252, 1, random_int(10000000, 99999999));   // Requires PRODUCT_ID, QUANTITY and REFERENCE_ID (own reference, optional, unless set to required on server side)
	//$res = $api->get_FinancialTransaction_ByDateRange(date('Y-m-d')); // Requires FROM_DATE and optional TO_DATE (default to start) with max 30 day difference
	//$res = $api->get_SalesTransaction_ByDateRange(date('Y-m-d')); // Requires FROM_DATE and optional TO_DATE (default to start) with max 30 day difference
	//$res = $api->get_ProductAvailability(1252);     // Requires PRODUCT_ID
	//$res = $api->get_AllProductAvailability();      // Has no parameters
	//$res = $api->get_ProductFormats();              // Has no parameters
	//$res = $api->get_MaxAllowedVouchersPerCall();   // Has no parameters
	//$res = $api->get_ProductInfo(1252);             // Requires PRODUCT_ID
	//$res = $api->get_SalesTransaction_ByTransactionId(1); // Requires TRANSACTION_ID
	//$res = $api->get_SalesTransaction_ByReferenceId('x');   // Requires REFERENCE_ID
	$res = $api->get_LastSale();                    // Has no parameters
	
	var_export($api);                               // Dump full API class
	var_dump($res);                                 // Dump the response. Equal to $api->response.
	var_export($api->response);                     // Response array, with SERVER_TRANSACTION_ID, SYSLOG_ID, RESULT code, ERROR_DESCRIPTION and CONTENT
	var_export($api->result);                       // Only the result ($api->response['CONTENT']) value. Contains "ERROR" if error occurred.
	var_export($api->errorDescription);             // Description of any error that occurred.
	var_export($api->ERROR);                        // Boolean indicating if error occurred during call process.

} catch (Throwable $e) {
	var_dump($e->getLine().':'.$e->getMessage());
}